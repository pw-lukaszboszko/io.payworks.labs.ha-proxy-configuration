# io.payworks.labs.ha-proxy-configuration

This project is a spike to verify if it&#39;s possible to generate HAProxy configuration with dynamic url-based routing

# Configuration
To configure HAProxy to use mapping file and regex based routing, you need to:
- specify servers 
- configure `use_backend` with map_reg(<file>)

config/haproxy.cfg:
```
  frontend http-frontend
    bind *:5000
    mode http
    use_backend %[path,map_reg(/etc/haproxy/routing.map)]
    default_backend server-default

  backend server-a
    mode http
    server server-a sample-a:8001

  backend server-b
    mode http
    server server-b sample-b:8002

  backend server-default
    mode http
    server server-b sample-default:8003
```

Mapping file will then specify regex-based rules for selecting target server with default fallback if no matches are found:

config/routing.map
```
^/v2/applications/[a-zA-Z-0-9-]*$ server-a
^/v2/partners/[a-zA-Z-0-9-]*$ server-b
```

This configuration will route any request to:
- `/v2/applications/<uuid>` -> server-a
- `/v2/partners/<uuid>` -> server-b
- `<anything else>` -> server-default


# Usage
1) execute `./run.sh` to build local docker images and start environment
2) verify routings:

```bash
curl http://localhost:5000/v2/applications/f07068ad-a6a8-4767-8b7d-614c1fc55b62
```
should return:
```json
{
  "message": "service-a"
}
```

```bash
curl http://localhost:5000/v2/partners/f07068ad-a6a8-4767-8b7d-614c1fc55b62
```
should return:
```json
{
  "message": "service-b"
}
```

```bash
curl http://localhost:5000/v2/merchants/f07068ad-a6a8-4767-8b7d-614c1fc55b62
```
should return:
```json
{
  "message": "service-default"
}
```