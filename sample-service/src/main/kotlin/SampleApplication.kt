@file:JvmName("SampleApplication")

package io.payworks.labs.haproxy.application

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun main(args: Array<String>) {
    embeddedServer(Netty, Integer.valueOf(args.getOrElse(1) { "8098" })) {
        routing {
            get("/*/*/*") {
                call.respond(Response(args.firstOrNull() ?: "unknown"))
            }
        }
        install(ContentNegotiation) {
            gson {
                setPrettyPrinting()
            }
        }
    }.start(wait = true)
}

data class Response(val message: String)

